//
//  main.swift
//  Advent of Code
//
//  Created by Emil Ramsdal on 01/12/2021.
//

import Foundation

func main() -> Void {
    
    let day: Day = Day6_2022()

    runDay(day: day)
}

func runDay(day: Day) {
    day.run()
}






main()
