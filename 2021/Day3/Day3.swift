//
//  Day3.swift
//  Advent of Code
//
//  Created by Emil Ramsdal on 02/12/2021.
//

import Foundation

class Day3 : Day {
    let currentDay = 3
    
    override init() {
        super.init(currentDay: currentDay, currentYear: 2021)
    }
    
    override init(test: Bool) {
        super.init(test: test)
    }
    
    override func taskOne(dayData: [String]) -> Int {
        var numOnes: [Int] = []
        var numZeros: [Int] = []
        
        for item1 in dayData {
            
            for (index, item) in item1.enumerated() {
                if !numOnes.indices.contains(index) {
                    numOnes.append(0)
                }
                if !numZeros.indices.contains(index) {
                    numZeros.append(0)
                }
                
                if item == "0" {
                    numZeros[index] += 1
                }
                if item == "1" {
                    numOnes[index] += 1
                }
            }
        }
        
        var mostNumbers: [Int]  = []
        var leastNumbers: [Int] = []
        var counter = 0;
        while counter < numOnes.count {
            if !leastNumbers.indices.contains(counter) {
                leastNumbers.append(0)
            }
            if !mostNumbers.indices.contains(counter) {
                mostNumbers.append(0)
            }
            
            if numOnes[counter] > numZeros[counter] {
                mostNumbers[counter] = 1
                leastNumbers[counter] = 0
            } else {
                mostNumbers[counter] = 0
                leastNumbers[counter] = 1
            }
            
            counter += 1
        }
        
        var leastNumberString = ""
        var mostNumberString = ""
        
        for item in mostNumbers {
            mostNumberString += String(item)
        }
        
        for item in leastNumbers {
            leastNumberString += String(item)
        }
        
        let leastDecimal = Int(leastNumberString, radix: 2)!
        let mostDecimal = Int(mostNumberString, radix: 2)!
        
        return leastDecimal * mostDecimal
    }
    
    override func taskTwo(dayData: [String]) -> Int {
        
        var numOnes: [Int] = []
        var numZeros: [Int] = []
        var numOnes2: [Int] = []
        var numZeros2: [Int] = []
        
        for item1 in dayData {
            
            for (index, item) in item1.enumerated() {
                if !numOnes.indices.contains(index) {
                    numOnes.append(0)
                    numOnes2.append(0)
                }
                if !numZeros.indices.contains(index) {
                    numZeros.append(0)
                    numZeros2.append(0)
                }
                
                if item == "0" {
                    numZeros[index] += 1
                    numZeros2[index] += 1
                    
                }
                if item == "1" {
                    numOnes[index] += 1
                    numOnes2[index] += 1
                }
            }
        }
        
        var counter = 0;
        
        var oxygen: [String] = dayData
        var co2: [String] = dayData
        
        
        var shouldSaveCo2 = true
        var shouldSaveOxygen = true
        while counter < numOnes.count {
            if (oxygen.count == 1) {
                shouldSaveOxygen = false
            }
            if (co2.count == 1) {
                shouldSaveCo2 = false
            }
            if shouldSaveOxygen {
                
                
                if numOnes[counter] >= numZeros[counter] {
                    
                    oxygen = oxygen.filter{ number in
                        let index2 = number.index(number.startIndex, offsetBy: counter) //will call succ 2 times
                        return number[index2] == "1"
                        
                    }
                    
                    
                } else if numOnes[counter] == numZeros[counter]{
                    
                    oxygen = oxygen.filter{ number in
                        let index2 = number.index(number.startIndex, offsetBy: counter) //will call succ 2 times
                        return number[index2] == "1"
                        
                    }
                    
                    
                } else {
                    
                    oxygen = oxygen.filter{ number in
                        let index2 = number.index(number.startIndex, offsetBy: counter) //will call succ 2 times
                        return number[index2] == "0"
                    }
                    
                    
                    
                }
            }
            if shouldSaveCo2 {
                
                
                if numOnes2[counter] >= numZeros2[counter] {
                    
                    
                    
                    co2 = co2.filter{ number in
                        let index2 = number.index(number.startIndex, offsetBy: counter) //will call succ 2 times
                        return number[index2] == "0"
                        
                    }
                    
                    
                } else if numOnes2[counter] == numZeros2[counter]{
                    
                    
                    co2 = co2.filter{ number in
                        let index2 = number.index(number.startIndex, offsetBy: counter) //will call succ 2 times
                        return number[index2] == "0"
                        
                    }
                    
                } else {
                    
                    
                    co2 = co2.filter{ number in
                        let index2 = number.index(number.startIndex, offsetBy: counter) //will call succ 2 times
                        return number[index2] == "1"
                        
                    }
                    
                }
            }
            
            let newValues = calculateNum(data: oxygen)
            numOnes = newValues[0]
            numZeros = newValues[1]
            
            let newValues2 = calculateNum(data: co2)
            numOnes2 = newValues2[0]
            numZeros2 = newValues2[1]
            counter += 1
        }
        
        let oxygenDemical = Int(oxygen[0], radix: 2)!
        let co2Decimal = Int(co2[0], radix: 2)!
        
        return oxygenDemical * co2Decimal
    }
    
    func calculateNum(data: [String]) -> [[Int]] {
        var numOnes: [Int] = []
        var numZeros: [Int] = []
        
        for item1 in data {
            
            for (index, item) in item1.enumerated() {
                if !numOnes.indices.contains(index) {
                    numOnes.append(0)
                }
                if !numZeros.indices.contains(index) {
                    numZeros.append(0)
                }
                
                if item == "0" {
                    numZeros[index] += 1
                }
                if item == "1" {
                    numOnes[index] += 1
                }
            }
        }
        
        return [numOnes, numZeros]
    }
    
    
    
}
