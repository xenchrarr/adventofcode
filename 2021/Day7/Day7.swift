//
//  Day7.swift
//  Advent of Code
//
//  Created by Emil Ramsdal on 07/12/2021.
//

import Foundation

class Day7 : Day {
    let currentDay = 7

    override init() {
        super.init(currentDay: currentDay, currentYear: 2021)
    }
    
    override init(test: Bool) {
        super.init(test: test)
    }
    
    
    override func taskOne(dayData: [String]) -> Int {
        var intValues = getDayDataInt(dayData: dayData)
        
        var lowestValue = 10000000
        
        intValues.sort()
        let highest = intValues.last!
        let lowest = intValues.first!
        
        for i in stride(from: lowest, to: highest, by: 1) {
            var current = 0
            for number in intValues {
                current += abs(number - i)
            }
            if abs(current) < lowestValue {
                lowestValue = abs(current)
            }
        }
        

        return lowestValue
    }
    
    override func taskTwo(dayData: [String]) -> Int {
        
        
        var intValues = getDayDataInt(dayData: dayData)
        
        var lowestValue = 100000000000000
        
        intValues.sort()
        let highest = intValues.last!
        let lowest = intValues.first!

        for i in stride(from: lowest, to: highest, by: 1) {
            var current = 0
            for number in intValues {
                current += getValue(number: number, target: i)
            }
            if abs(current) < lowestValue {
                lowestValue = abs(current)
            }
        }
        

        return lowestValue
           
    }
    
    func getDayDataInt(dayData: [String]) -> [Int] {
        var value: [Int] = []
        for item in dayData.first!.components(separatedBy: ",") {
            value.append(Int(item)!)
        }
        
        return value
    }
    
    func getValue(number: Int, target: Int) -> Int{
        
        let steps = abs(number - target)
        var total = 0
        
        for num in 0...steps {
            total += num
        }
        
        return total
    }


}

