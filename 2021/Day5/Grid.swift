//
//  Grid.swift
//  Advent of Code
//
//  Created by Emil Ramsdal on 06/12/2021.
//

import Foundation

class Grid {
    var grid: [[Int]] = [[]]
    
    init() {
        grid = Array(repeating: Array(repeating: 0, count: 1000), count: 1000)

    }
    
    func addLine(line: Line) {
        for point in line.points {
            grid[point.x][point.y] += 1
        }
    }
    
    
    func getNumberOfPointsWithMoreThanTwo() -> Int{
        var numberOfPoints = 0
        for outerArray in grid {
            for point in outerArray {
                if point >= 2 {
                    numberOfPoints += 1
                }
            }
        }
        
        return numberOfPoints
    }
    
}
