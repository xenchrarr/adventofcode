//
//  Line.swift
//  Advent of Code
//
//  Created by Emil Ramsdal on 06/12/2021.
//

import Foundation

class Line {
    
    var points: [Point] = []
    var diagonal: Bool
    
    init(startStopString: String, diagonal: Bool) {
        self.diagonal = diagonal
        createPoints(stringPoints: startStopString)
        
        
    }
    
    func createPoints(stringPoints: String) {
        let stringPoints = stringPoints.components(separatedBy: " -> ")
        let startingPointString = stringPoints.first!.components(separatedBy: ",")
        let endingPointString = stringPoints.last!.components(separatedBy: ",")
        
        let startingPoint = Point(x: Int(startingPointString.first!.filter { !$0.isNewline })!,
                                  y: Int(startingPointString.last!.filter { !$0.isNewline })!)
        
        let endingPoint = Point(x: Int(endingPointString.first!.filter { !$0.isNewline })!,
                                y: Int(endingPointString.last!.filter { !$0.isNewline })!)
        
        
        if startingPoint.x == endingPoint.x {
            //    vertical
            let x = startingPoint.x
            var y = startingPoint.y
            
            let addingValue = startingPoint.y < endingPoint.y ? 1 : -1
            
            while y != endingPoint.y {
                
                points.append(Point(x: x, y: y))
                
                
                y += addingValue
            }
            
            points.append(endingPoint)
        }
        
        else if startingPoint.y == endingPoint.y {
            
            var x = startingPoint.x
            let y = startingPoint.y
            
            let addingValue = startingPoint.x < endingPoint.x ? 1 : -1
            
            while x != endingPoint.x {
                
                points.append(Point(x: x, y: y))
                
                
                x += addingValue
            }
            
            points.append(endingPoint)
        } else {
            if diagonal {
                var x = startingPoint.x
                var y = startingPoint.y
                
                let addingValueY = startingPoint.y < endingPoint.y ? 1 : -1
                let addingValueX = startingPoint.x < endingPoint.x ? 1 : -1
                
                while x != endingPoint.x  && y != endingPoint.y{
                    if x == endingPoint.x && y == endingPoint.y {
                        break
                    }
                    
                    points.append(Point(x: x, y: y))
                    
                    
                    x += addingValueX
                    y += addingValueY
                }
                
                points.append(endingPoint)
            }
            
        }
        
    }
}
