//
//  Point.swift
//  Advent of Code
//
//  Created by Emil Ramsdal on 06/12/2021.
//

import Foundation


class Point {
    let x: Int
    let y: Int
    
    
    init(x: Int, y: Int) {
        self.x = x
        self.y = y
    }
}
