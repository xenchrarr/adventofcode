//
//  Day5.swift
//  Advent of Code
//
//  Created by Emil Ramsdal on 06/12/2021.
//

import Foundation

class Day5 : Day {
    let currentDay = 5

    override init() {
        super.init(currentDay: currentDay, currentYear: 2021)
    }
    
    override init(test: Bool) {
        super.init(test: test)
    }
    
    
    override func taskOne(dayData: [String]) -> Int {
        
        let lines: [Line] = getLinesFromCoordinatesInput(values: dayData, diagonal: false)
        
        let grid = Grid()
        
        for line in lines {
            grid.addLine(line: line)
        }
        
        
        
        return grid.getNumberOfPointsWithMoreThanTwo()
    }
    
    override func taskTwo(dayData: [String]) -> Int {
        
        let lines: [Line] = getLinesFromCoordinatesInput(values: dayData, diagonal: true)
        
        let grid = Grid()
        
        for line in lines {
            grid.addLine(line: line)
        }
        
        
        
        return grid.getNumberOfPointsWithMoreThanTwo()
           
    }

    func getLinesFromCoordinatesInput(values: [String], diagonal: Bool) -> [Line] {
        var lines: [Line] = []
        for item in values {
            lines.append(Line( startStopString: item, diagonal: diagonal))
        }
        
        return lines
    }

}
