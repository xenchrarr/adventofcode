//
//  Day4.swift
//  Advent of Code
//
//  Created by Emil Ramsdal on 04/12/2021.
//

import Foundation

class Day4 : Day {
    let currentDay = 4
    
    override init() {
        super.init( currentDay: currentDay, currentYear: 2021)
    }
    
    override init(test: Bool) {
        super.init(test: test)
    }
    
    
    override func taskOne(dayData: [String]) -> Int {
        let drawnNumbers = dayData[0].split(separator: ",")

        var newDayData = dayData
        newDayData.removeFirst()
        
        let newBoards = createBoards(data: newDayData)
        
        var boards: [Board] = []
        for board in newBoards {
            boards.append(Board(data: board))
        }
        
        
        
        for number in drawnNumbers {
            for board in boards {
                board.setNumberChecked(number: Int(number)!)
                
                if board.isWinning() {
                    return board.getUnmarkedSum() * Int(number)!
                }
            }
        }
        
        return 0
    }
    
    override func taskTwo(dayData: [String]) -> Int {
        let drawnNumbers = dayData[0].split(separator: ",")

        var newDayData = dayData
        newDayData.removeFirst()
        
        let newBoards = createBoards(data: newDayData)
        
        var boards: [Board] = []
        for board in newBoards {
            boards.append(Board(data: board))
        }
        
        
        var winningBoards = 0
        for number in drawnNumbers {
            for board in boards {
                board.setNumberChecked(number: Int(number)!)
                
                if board.isDone {
                    continue
                }
                

                if board.isWinning() {
                    winningBoards += 1
                    if winningBoards == boards.count  {
                        return board.getUnmarkedSum() * Int(number)!
                    }
                }
            }
        }
        
        return 0
        
    }
    
    func createBoards(data: [String]) -> [[String]]{
        
        var boardLines: [[String]] = []
        var current: [String] = []
        for line in data {
            if line.isEmpty {
                if current.count > 0 {
                    boardLines.append(current)
                }
                    
                current = []
                continue
            }
            
            current.append(line)
            
        }
        
        
        if current.count > 0 {
            boardLines.append(current)
        }
        return boardLines
        
    }
    
}
