//
//  Board.swift
//  Advent of Code
//
//  Created by Emil Ramsdal on 04/12/2021.
//

import Foundation

class Board {
    
    var boardMap: [[BoardElement]] = [[]]
    var isDone = false
    
    init(data: [String]) {

        for (row, line) in data.enumerated() {
            let values = line.components(separatedBy: [",", " ", "!",".","?"])
//            boardMap.append([])
            
            for value in values {
                let newValue = value.filter { !$0.isWhitespace }
                if newValue.isEmpty {
                    continue
                }
                if boardMap.count == row {
                    boardMap.append([])
                }
                boardMap[row].append( BoardElement(number: Int(newValue)!))
            }
        }
        
    }
    
    func isWinning() -> Bool {
        if isDone {
            return isDone
        }
        
        isDone = checkRows() || checkColumns()
        
        return isDone
    }
    
    func getIsDone() -> Bool {
        return isDone
    }
    
    func checkRows() -> Bool {
        for row in boardMap {
            var isWinning = 0
            for item in row {
                if item.isChecked {
                    isWinning += 1
                }
            }
            if isWinning == boardMap[0].count {
                return true
            }
        }
        
        return false
    }
    
    
    func checkColumns() -> Bool {
        var column: Int = 0
        var row: Int = 0
        
        while column < boardMap.count {
            var isWinning = 0
            while row < boardMap.count {
                if boardMap[row][column].isChecked {
                    isWinning += 1
                }
                
                row += 1
            }
            
            if isWinning == boardMap.count {
                return true
            }
            
            row = 0
            column += 1
        }
        
        return false
    }
    
    
    func setNumberChecked(number: Int) {
        for row in boardMap {
            for item in row {
                if item.value == number {
                    item.setChecked(checked: true)
                }
            }
        }
    }
    
    func getUnmarkedSum() -> Int {
        var sum = 0
        for row in boardMap {
            for item in row {
                if !item.isChecked {
                    sum += item.value
                }
            }
        }
        return sum
    }
}
