//
//  BoardElement.swift
//  Advent of Code
//
//  Created by Emil Ramsdal on 04/12/2021.
//

import Foundation

class BoardElement {
    var value: Int
    var isChecked: Bool
    
    init(number: Int) {
        value = number
        isChecked = false
    }
    
    func setChecked(checked: Bool) -> Void {
        isChecked = checked
    }
    
}
