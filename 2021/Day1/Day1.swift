//
//  Day1.swift
//  Advent of Code
//
//  Created by Emil Ramsdal on 01/12/2021.
//

import Foundation


class Day1 : Day {
    let currentDay = 1

    override init() {
        super.init(currentDay: currentDay, currentYear: 2021)
    }
    
    override init(test: Bool) {
        super.init(test: test)
    }
    
    
    override func taskOne(dayData: [String]) -> Int {
        var counter = 0
        for (index, item) in dayData.enumerated() {
            if index == 0 {
                continue
            }
            
            if Int(item )! > Int(dayData[index-1] )! {
                counter += 1
            }
        }
        
        return counter
    }
    
    override func taskTwo(dayData: [String]) -> Int {
        var counter = 0
        var index = 0
        while index < dayData.count {
            if index + 3 >= dayData.count {
                break
            }
            
            if shouldAdd(val1: Int(dayData[index])!,
                         val2: Int(dayData[index + 1]) ?? 0,
                         val3: Int(dayData[index + 2]) ?? 0,
                         val4: Int(dayData[index + 3]) ?? 0) {
                counter += 1
            }
            
            index += 1
        }
        
        
        return counter
           
    }

    func shouldAdd(val1: Int, val2: Int, val3: Int, val4: Int) -> Bool {
        let a = val1 + val2 + val3
        let b = val2 + val3 + val4
        return b > a
    }

}
