//
//  Fish.swift
//  Advent of Code
//
//  Created by Emil Ramsdal on 06/12/2021.
//

import Foundation

class Fish {
    var daysToReproduce: Int
    
    init(days: Int) {
        daysToReproduce = days
    }
    
    
    func countDown() -> Bool{
        
        daysToReproduce -= 1
        
        if daysToReproduce >= 0 {
            return false
        }
        
        daysToReproduce = 6
        return true
        
    }
}
