//
//  Day6.swift
//  Advent of Code
//
//  Created by Emil Ramsdal on 06/12/2021.
//

import Foundation

class Day6 : Day {
    let currentDay = 6

    override init() {
        super.init(currentDay: currentDay, currentYear: 2021)
    }
    
    override init(test: Bool) {
        super.init(test: test)
    }
    
    
    override func taskOne(dayData: [String]) -> Int {

        var fishes = getFish(days: getFishNumbers(items: dayData))

        var counter = 0
        while counter < 80 {
            var newDayFishes: [Fish] = []
            for fish in fishes {
                if fish.countDown() {
                    newDayFishes.append(Fish(days: 8))
                }
            }
            fishes.append(contentsOf: newDayFishes)
            
            counter += 1
            
            
        }

     
        
        return fishes.count
    }
    
    override func taskTwo(dayData: [String]) -> Int {
        
        var fish2: [Int] = Array(repeating: 0, count: 9)
        for item in getFishNumbers(items: dayData) {
            fish2[item] += 1
        }
        

        var counter = 0
        while counter < 256 {
            let day0 = fish2.first!
            fish2.removeFirst()
            fish2[6] += day0
            fish2.append(day0)
            counter += 1
            
        }

        var count = 0
        for item in fish2 {
            count += item
        }
        
        return count
           
    }
    
    func getFishNumbers(items: [String]) -> [Int] {
        let values = items.first!.components(separatedBy: ",")
        
        var fish: [Int] = []
        for item in values {
            fish.append(Int(item)!)
        }
        return fish
    }


    func getFish(days: [Int]) -> [Fish]{
        var fish: [Fish] = []
        for item in days {
            fish.append(Fish(days: item))
        }
        
        return fish
    }

}

