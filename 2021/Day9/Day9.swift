//
//  Day9.swift
//  Advent of Code
//
//  Created by Emil Ramsdal on 09/12/2021.
//

import Foundation

class Day9 : Day {
    let currentDay = 9

    override init() {
        super.init(currentDay: currentDay, currentYear: 2021)
    }
    
    override init(test: Bool) {
        super.init(test: test)
    }
    
    
    override func taskOne(dayData: [String]) -> Int {
        
        let intArray = splitString(data: dayData)
        
        var total = 0
        for (xIndex, x) in intArray.enumerated() {
            for (yIndex, y) in x.enumerated() {
                
                // left upper corner
                if yIndex == 0 && xIndex == 0{
                    if y < x[yIndex + 1] && y < intArray[xIndex + 1][yIndex] {
                        total += (y + 1)
                 
                    }
                    continue
                }
                
                // top right corner
                
                if xIndex == 0 && yIndex == x.count - 1 {
                    if y < x[yIndex - 1] && y < intArray[xIndex + 1][yIndex] {
                        total += (y + 1)
              
                    }
                    continue
                }
                
                // top row
                
                if  xIndex == 0 {
                    if y < x[yIndex + 1] && y < x[yIndex - 1] && y < intArray[xIndex + 1][yIndex] {
                        total += (y + 1)
                     
                    }
                    continue
                }
                

                
                // normal left
                
                if yIndex == 0 && xIndex < intArray.count - 1{
                    if y < x[yIndex + 1] && y < intArray[xIndex + 1][yIndex] && y < intArray[xIndex - 1][yIndex] {
                        total += (y + 1)
                       
                    }
                    continue
                }
                
                
                // normal right
                
                if yIndex == x.count - 1{
                    if y < x[yIndex - 1] && y < intArray[xIndex + 1][yIndex] && y < intArray[xIndex - 1][yIndex] {
                        total += (y + 1)
                    }
                    continue
                }
                
                // bottom left corner
                
                if xIndex == x.count - 1 && yIndex == 0{
                    if y < x[yIndex + 1] && y < intArray[xIndex - 1][yIndex] {
                        total += (y + 1)
                        
                    }
                    continue
                }
                
                // bottom right corner
                
                if yIndex == x.count - 1 && xIndex == 0{
                    if y < x[yIndex - 1] && y < intArray[xIndex - 1][yIndex] {
                        total += (y + 1)
                        
                    }
                    continue
                }
                
                //bottom row
                
                if xIndex == intArray.count - 1 {
                    if y < x[yIndex - 1] && y < intArray[xIndex - 1][yIndex] && y < x[yIndex + 1] {
                        total += (y + 1)
                        
                    }
                    continue
                }
                    
                // normal
                if yIndex > 0 && xIndex > 0 && xIndex < x.count && yIndex < intArray.count - 1 {
                    if y < x[yIndex - 1] && y < intArray[xIndex - 1][yIndex] && y < x[yIndex + 1] && y < intArray[xIndex + 1][yIndex] {
                        total += (y + 1)
                        
                    }
                    continue
                }
                
            }
        }
        return total
    }
    
    override func taskTwo(dayData: [String]) -> Int {
        return 0
           
    }
    
    func splitString(data: [String]) -> [[Int]] {
        
        var newData: [[Int]] = []
        

        for line in data {
            var intArray: [Int] = []
            let charArray = line.map({ String($0) })
            
            for letter in charArray {
                intArray.append(Int(letter)!)
            }
            newData.append(intArray)
        }
        
        return newData
    }


}

