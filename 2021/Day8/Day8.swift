//
//  Day8.swift
//  Advent of Code
//
//  Created by Emil Ramsdal on 08/12/2021.
//

import Foundation

class Day8 : Day {
    let currentDay = 8

    override init() {
        super.init(currentDay: currentDay, currentYear: 2021)
    }
    
    override init(test: Bool) {
        super.init(test: test)
    }
    
    
    override func taskOne(dayData: [String]) -> Int {
        let newInput = splitUpInput(input: dayData)

        let intArr = [2, 4, 3, 7]

        var total = 0
        
        for line in newInput {
            for item in line[1] {
                if intArr.contains(item.count) {
                    total += 1
                }
            }
        }
        
        
        return total
    }
    
    override func taskTwo(dayData: [String]) -> Int {
        
        return 0
           
    }

    func splitUpInput(input: [String]) -> [[[String]]] {
        var newItems: [[[String]]] = []
        
        for item in input {
            let splitted = item.components(separatedBy: " | ")
            let firstPart = splitted.first!.components(separatedBy: " ")
            let secondPart = splitted.last!.components(separatedBy: " ")
            let newItem: [[String]] = [firstPart, secondPart]
            newItems.append(newItem)
            
            
        }
        
        return newItems
    }
    
    func getSumFromLine(input: [[String]]) -> Int {
        
        
        
        
        return 0
    }

}


