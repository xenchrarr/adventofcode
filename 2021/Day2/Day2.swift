//
//  Day2.swift
//  Advent of Code
//
//  Created by Emil Ramsdal on 01/12/2021.
//

import Foundation

class Day2 : Day {

    let currentDay = 2
    
    override init() {
        super.init(currentDay: currentDay, currentYear: 2021)
    }
    
    override init(test: Bool) {
        super.init(test: test)
    }
    

    
    override func taskOne(dayData: [String]) -> Int {
        var horizontal = 0
        var depth = 0
        
        let dayDataTuple = createTuple(data: dayData)
        
        for item in dayDataTuple {
            let newItem = item
            if newItem.0 == "forward" {
                horizontal += newItem.1
            }
            
            if newItem.0 == "up" {
                depth -= newItem.1
            }
            
            if newItem.0 == "down" {
                depth += newItem.1
            }
            

            
        }
        return depth * horizontal
    }
    
    override func taskTwo(dayData: [String]) -> Int {
        
        var horizontal = 0
        var depth = 0
        var aim = 0
        
        let dayDataTuple = createTuple(data: dayData)
        
        for item in dayDataTuple {
            let newItem = item
            if newItem.0 == "forward" {
                horizontal += newItem.1
                depth += (aim * newItem.1)
            }
            
            if newItem.0 == "up" {
                aim -= newItem.1
            }
            
            if newItem.0 == "down" {
                aim += newItem.1
            }
            

            
        }
        return depth * horizontal
    }
    
    
    func createTuple(data: [String]) -> [(String, Int)] {
        var linesTuple: [(direction: String, value: Int)] = []
        for item in data {
            let lineData = item.components(separatedBy: [",", " ", "!",".","?"])
            let direction = lineData[0].filter { !$0.isWhitespace }
            let lineTuple = (direction: direction, value: Int(lineData[1].filter { !$0.isWhitespace }) ?? 0)
            
            
            linesTuple += [lineTuple]
        }
        
        return linesTuple
    }
    
    
}
