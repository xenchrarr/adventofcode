//
//  Day10.swift
//  Advent of Code
//
//  Created by Emil Ramsdal on 10/12/2021.
//

import Foundation


class Day10 : Day {
    let currentDay = 10
    let openings = ["(", "[", "{", "<"]
    let closings = [")", "]", "}", ">"]
    let points: [Int] = [3, 57, 1197, 25137]
    
    let points2: [Int] = [1, 2, 3, 4]

    override init() {
        super.init(currentDay: currentDay, currentYear: 2021)
    }
    
    override init(test: Bool) {
        super.init(test: test)
    }
    
    
    override func taskOne(dayData: [String]) -> Int {
        var score = 0
        for line in dayData {
            
            var expectedClosings: [String] = []
            let charArray = line.map({ String($0) })
            for letter in charArray {
                if openings.contains(letter) {
                    expectedClosings.append(closings[openings.firstIndex(of: letter)!])
                
                } else if (closings.contains(letter)) {
                    if expectedClosings.last == letter {
                        expectedClosings.removeLast()
                    } else {
                        score += points[closings.firstIndex(of: letter)!]
                        break
                    }
                } else {
                    print ("Error")
                    break
                }
                
            }
        }
        return score
    }
    
    override func taskTwo(dayData: [String]) -> Int {
        var scores: [Int] = []
        for line in dayData {
            var isBroken = false
            var expectedClosings: [String] = []
            let charArray = line.map({ String($0) })
            for letter in charArray {
                if openings.contains(letter) {
                    expectedClosings.append(closings[openings.firstIndex(of: letter)!])
                
                } else if (closings.contains(letter)) {
                    if expectedClosings.last == letter {
                        expectedClosings.removeLast()
                    } else {
                        isBroken = true
                        break
                        
                    }
                } else {
                    isBroken = true
                    print ("Error")
                    break
                }
                
            }
            if isBroken {
                continue
            }
            
            if expectedClosings.count > 0 {
                var score = 0
                expectedClosings.reverse()
                for letter in expectedClosings {
                    score = (score * 5) + points2[closings.firstIndex(of: letter)!]
                }
                
                scores.append(score)
            }
            

        }
        
        scores.sort()
        
        return scores[Int(scores.count/2)]
    }

}

