//
//  Day.swift
//  Advent of Code
//
//  Created by Emil Ramsdal on 01/12/2021.
//

import Foundation

class Day : DayProtocol {
    var day = 0
    let fileReader: ReadFile?
    let filePath: String?
    var data: [String]?

    init( currentDay: Int, currentYear: Int) {
        print("Reading file")
        self.fileReader = ReadFile()
        self.filePath = "\(currentYear)/Day\(currentDay)/data.txt"
        self.data = []
        self.day = currentDay
        self.data = fileReader!.readFileLineByLine(filePath: filePath!)
        
    }
    
    // Empty constructor so we can run tests
    init() {
        self.fileReader = nil
        self.filePath = nil
        self.data = nil
    }
    init(test: Bool) {
        
        self.fileReader = nil
        self.filePath = nil
        self.data = nil
        
    }
    
    func run() {
        print("Day: \(getDay())")
        let taskOne = taskOne(dayData: self.data ?? [])
        print("Task One \(taskOne)")
        let taskTwo = taskTwo(dayData: self.data ?? [])
        print("Task two \(taskTwo)")
    }
    
    func getDay() -> Int {
        return day
    }
    
    func taskOne(dayData: [String]) -> Int {
        abort()
    }
    
    func taskTwo(dayData: [String]) -> Int {
        abort()
    }
    
    
}
