//
//  DayProtocol.swift
//  Advent of Code
//
//  Created by Emil Ramsdal on 01/12/2021.
//

import Foundation

protocol DayProtocol {
    var day: Int { get }
    var filePath: String? { get }
    func run() -> Void
    func taskOne(dayData: [String]) -> Int
    func taskTwo(dayData: [String]) -> Int
    func getDay() -> Int
}
