//
//  ReadFile.swift
//  Advent of Code
//
//  Created by Emil Ramsdal on 01/12/2021.
//

import Foundation


class ReadFile {
    
    let basePath = "/Users/emilramsdal/workspace/adventofcode"
    
    func readFileLineByLine(filePath: String) -> [String] {
        
        var lines: [String] = []
        let path = basePath + "/" + filePath
        
        
        // make sure the file exists
        guard FileManager.default.fileExists(atPath: path) else {
            preconditionFailure("file expected at \(path) is missing")
        }

        // open the file for reading
        // note: user should be prompted the first time to allow reading from this location
        guard let filePointer:UnsafeMutablePointer<FILE> = fopen(path,"r") else {
            preconditionFailure("Could not open file at \(path)")
        }

        // a pointer to a null-terminated, UTF-8 encoded sequence of bytes
        var lineByteArrayPointer: UnsafeMutablePointer<CChar>? = nil

        // the smallest multiple of 16 that will fit the byte array for this line
        var lineCap: Int = 0

        // initial iteration
        var bytesRead = getline(&lineByteArrayPointer, &lineCap, filePointer)

        defer {
            // remember to close the file when done
            fclose(filePointer)
        }

        while (bytesRead > 0) {
            
            // note: this translates the sequence of bytes to a string using UTF-8 interpretation
            let lineAsString = String.init(cString:lineByteArrayPointer!)
            
            lines.append(lineAsString.filter { !$0.isNewline })
            // updates number of bytes read, for the next iteration
            bytesRead = getline(&lineByteArrayPointer, &lineCap, filePointer)
        }
        
        

        return lines
    }
}
