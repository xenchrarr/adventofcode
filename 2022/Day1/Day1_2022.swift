//
//  Day1.swift
//  Advent of Code
//
//  Created by Emil Ramsdal on 01/11/2022.
//

import Foundation

class Day1_2022 : Day {
    let currentDay = 1

    override init() {
        super.init(currentDay: currentDay, currentYear: 2022)
    }
    
    override init(test: Bool) {
        super.init(test: test)
    }
    
    override func taskOne(dayData: [String]) -> Int {
        
        var currentHighest = 0
        
        var current = 0
        for item in dayData {
            if item == "" {
                if current > currentHighest {
                    currentHighest = current

                }
                
                current = 0
                continue
            }
            
            current = current + Int(item)!
        }
        return currentHighest
    }
    
    override func taskTwo(dayData: [String]) -> Int {
        
        var currentHighest = [0,0,0]
        
        var current = 0
        for (index, item) in dayData.enumerated() {
            if item == "" || index == dayData.count - 1{
                let num = minIndex(someArray: currentHighest)
                if current == 0 {
                    current = Int(item)!
                }
                if current > currentHighest[num] {
    
                    currentHighest[num] = current
                }
                
                
                current = 0
                continue
            }
            
            current = current + Int(item)!
        }
        return currentHighest.reduce(0, +)
    }
    
    
    func minIndex(someArray: [Int]) -> Int {
        return someArray.indices.min { someArray[$0] < someArray[$1] }!
    }
}
