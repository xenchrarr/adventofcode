//
//  Day3_2022.swift
//  Advent of Code
//
//  Created by Emil Ramsdal on 02/12/2022.
//

import Foundation

class Day3_2022 : Day {
    let currentDay = 3
    
    let alphabet = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
    
    override init() {
        super.init(currentDay: currentDay, currentYear: 2022)
    }
    
    override init(test: Bool) {
        super.init(test: test)
    }
    
    override func taskOne(dayData: [String]) -> Int {
        
        var total = 0
        
        for var line in dayData {
            let halfLength = line.count / 2

               let index = line.index(line.startIndex, offsetBy: halfLength)
            line.insert("-", at: index)
               let result = line.split(separator: "-")
            
            for letter in result[0] {
                if result[1].contains(letter) {
                    var i = 1
                    var finish = false
                    for k in alphabet {
                 
                        if (String(letter) == k) {
                            total = total + i
                            finish = true
                            break
                        }
                        i = i + 1
                    }
                    if finish {
                        break
                    }
                }
            }
        }
        return total
    }
    
    override func taskTwo(dayData: [String]) -> Int {
        
        var index = 0
        var total = 0;
        while (index < dayData.count) {
            for letter in dayData[index] {
                if dayData[index + 1].contains(letter) && dayData[index + 2].contains(letter) {
                    var i = 1
                    var finish = false
                    for k in alphabet {
                 
                        if (String(letter) == k) {
                            total = total + i
                            finish = true
                            break
                        }
                        i = i + 1
                    }
                    
                    if finish {
                        break
                    }
                }
            }
            
            
            
            index = index + 3
        }
        return total
    }
}
