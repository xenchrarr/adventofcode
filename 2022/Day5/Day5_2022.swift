//
//  Day5_2022.swift
//  Advent of Code
//
//  Created by Emil Ramsdal on 05/12/2022.
//

import Foundation

class Day5_2022 : Day {
    let currentDay = 3
    

    override init() {
        super.init(currentDay: currentDay, currentYear: 2022)
    }
    
    override init(test: Bool) {
        super.init(test: test)
    }
    
    override func taskOne(dayData: [String]) -> Int {
        
        var total = 0
        
        for var line in dayData {
            print(line)
            let items = line.split(separator: " ")
            print(items)
            
            var num = 0
            for ( index, character) in line.enumerated() {
                print (character)
                if character == " " {
                    num = num + 1
                }
            }
        }
        return total
    }
    
    override func taskTwo(dayData: [String]) -> Int {
        
        var index = 0
        var total = 0;

        return total
    }
}

