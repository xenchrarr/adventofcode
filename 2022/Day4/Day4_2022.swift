//
//  Day4_2022.swift
//  Advent of Code
//
//  Created by Emil Ramsdal on 03/12/2022.
//

import Foundation

class Day4_2022 : Day {
    let currentDay = 4
    
    override init() {
        super.init(currentDay: currentDay, currentYear: 2022)
    }
    
    override init(test: Bool) {
        super.init(test: test)
    }
    
    override func taskOne(dayData: [String]) -> Int {
        
        var total = 0
        for line in dayData {
            let sections = line.split(separator: ",")
            
            let first = sections[0].split(separator: "-")
            let last = sections[1].split(separator: "-")
            
            if (
                Int(first[0])! <= Int(last[0])!
                &&
                Int(first[1])! >= Int(last[1])!
            )
                ||
                (
                    Int(first[0])! >= Int(last[0])!
                    &&
                    Int(first[1])! <= Int(last[1])!) {
                total = total + 1
            }
            
        }
        
        return total
    }
    
    override func taskTwo(dayData: [String]) -> Int {
        var total = 0
        for line in dayData {
            let sections = line.split(separator: ",")
            
            let first = sections[0].split(separator: "-")
            let last = sections[1].split(separator: "-")
            
            if (
                Int(first[0])! <= Int(last[0])!
                &&
                Int(first[1])! >= Int(last[1])!
            )
                ||
                (
                    Int(first[0])! >= Int(last[0])!
                    &&
                    Int(first[1])! <= Int(last[1])!)
            ||
                (Int(first[0])! <= Int(last[0])!
                 &&
                 Int(first[1])! >= Int(last[0])!)
                
                ||
                    (Int(first[0])! >= Int(last[0])!
                     &&
                     Int(first[0])! <= Int(last[1])!)
            {
                total = total + 1
            }
            
        }
        
        return total
    }
    
    
}
