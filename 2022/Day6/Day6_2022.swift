//
//  Day6_2022.swift
//  Advent of Code
//
//  Created by Emil Ramsdal on 07/12/2022.
//

import Foundation

class Day6_2022 : Day {
    let currentDay = 6
    
    
    override init() {
        super.init(currentDay: currentDay, currentYear: 2022)
    }
    
    override init(test: Bool) {
        super.init(test: test)
    }
    
    
    override func taskOne(dayData: [String]) -> Int {
        
        let line = dayData[0]
        

        
        let lineArray = Array(line)
        var index = 0
        
        var finished = false
        
        for char in lineArray {
            var key : Array<Character> = Array(repeating: "?", count: 4 )
            // myArray.filter{$0 == "a"}.count
            if char == lineArray[index] {
                
            }
            
//            key[0] = char
            
            var counter = 0
            while counter < 4 {
                
                key[counter] = lineArray[index + counter]
                
                counter = counter + 1
                
                if key.filter({$0 == lineArray[index + counter]}).count > 0 {
                    break
                }
                
                if counter == 4 {
                    finished = true
                    break
                }
                
            }
            
            if finished {
                break
            }
            index = index + 1
        }
        
        return index + 4
    }
    
    override func taskTwo(dayData: [String]) -> Int {
        let line = dayData[0]
        

        
        let lineArray = Array(line)
        var index = 0
        
        var finished = false
        
        for char in lineArray {
            var key : Array<Character> = Array(repeating: "?", count: 14 )
            // myArray.filter{$0 == "a"}.count
            if char == lineArray[index] {
                
            }
            
//            key[0] = char
            
            var counter = 0
            while counter < 14 {
                
                key[counter] = lineArray[index + counter]
                
                
                if key.filter({$0 == lineArray[index + counter]}).count > 1 {
                    break
                }
                
                counter = counter + 1
                
                if counter == 14 {
                    finished = true
                    break
                }
                
            }
            
            if finished {
                break
            }
            index = index + 1
        }
        
        return index + 14
    }
    
    

    
    
}
