//
//  Day2_2022.swift
//  Advent of Code
//
//  Created by Emil Ramsdal on 02/12/2022.
//

import Foundation

class Day2_2022: Day {
    let currentDay = 2
    
    let points = [
        "A": [
            "X": 4,
            "Y": 8,
            "Z": 3
        ],
        "B" : [
            "X": 1,
            "Y": 5,
            "Z": 9
        ],
        "C": [
            "X": 7,
            "Y": 2,
            "Z": 6
        ]
    ]
    
    let winMapper = [
        "A": [
            "X": "Z",
            "Y": "X",
            "Z": "Y"
        ],
        "B" : [
            "X": "X",
            "Y": "Y",
            "Z": "Z"
        ],
        "C": [
            "X": "Y",
            "Y": "Z",
            "Z": "X"
        ]
    ]
    
    override init() {
        super.init(currentDay: currentDay, currentYear: 2022)
    }
    
    override init(test: Bool) {
        super.init(test: test)
    }
    
    override func taskOne(dayData: [String]) -> Int {
        
        
        var total = 0
        for item in dayData {
            total = total + getMatchPoint(match: item)
        }
        
        return total
    }
    
    override func taskTwo(dayData: [String]) -> Int {
        var total = 0
        for item in dayData {
            total = total + getMatchPoint2(match: item)
        }
        
        
        return total
    }
    func getMatchPoint(match: String) -> Int {
        
        let item = match.components(separatedBy: " ")
        
        
        let point =  points[item[0]]![item[1]]!
        
        return point
    
    }
    
    func getMatchPoint2(match: String) -> Int {
        
        let item = match.components(separatedBy: " ")
        
        let letter = winMapper[item[0]]![item[1]]!
        let point =  points[item[0]]![letter]!
        
        return point
    
    }
    
    
    
    
}
