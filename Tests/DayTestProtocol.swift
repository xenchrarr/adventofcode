//
//  DayTestProtocol.swift
//  AdventOfCodeTests
//
//  Created by Emil Ramsdal on 01/12/2021.
//

import XCTest

protocol DayTest : XCTestCase {
    func testTaskOne() throws -> Void
    func testTaskTwo() throws -> Void
    
}
