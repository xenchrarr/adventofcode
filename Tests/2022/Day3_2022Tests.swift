//
//  Day3_2022Tests.swift
//  AdventOfCodeTests
//
//  Created by Emil Ramsdal on 02/12/2022.
//

import XCTest

final class Day3_2022Tests: XCTestCase, DayTest{

    var day: Day = Day3_2022(test: true)
    
    let data: [String] = ["vJrwpWtwJgWrhcsFMMfFFhFp",
                          "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
                          "PmmdzqPrVvPwwTWBwg",
                          "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
                          "ttgJtRGJQctTZtZT",
                          "CrZsJsPPZsGzwwsLwLmpwMDw"]
    
    
    func testTaskOne() throws {
        print("starting taskOne")
        let result = self.day.taskOne(dayData: data)
        
        XCTAssertEqual(result, 157)
    }
    
    func testTaskTwo() throws {
        print("starting taskTwo")
        let result = self.day.taskTwo(dayData: data)
        
        XCTAssertEqual(result, 70)
    }
    

}
