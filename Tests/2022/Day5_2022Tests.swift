//
//  Day5_2022Tests.swift
//  AdventOfCodeTests
//
//  Created by Emil Ramsdal on 05/12/2022.
//

import XCTest

final class Day5_2022Tests: XCTestCase, DayTest {

    
    var day: Day = Day5_2022(test: true)
    
    
    let data = [
    "    [D]",
    "[N] [C]",
    "[Z] [M] [P]",
     "1   2   3",
"",
        "move 1 from 2 to 1",
        "move 3 from 1 to 3",
        "move 2 from 2 to 1",
        "move 1 from 1 to 2"
    ]
    
    
    func testTaskOne() throws {
        print("starting taskOne")
        let result = self.day.taskOne(dayData: data)
        
        XCTAssertEqual(result, 24000)
    }
    
    func testTaskTwo() throws {
        print("starting taskTwo")
        let result = self.day.taskTwo(dayData: data)
        
        XCTAssertEqual(result, 45000)
    }
    



}
