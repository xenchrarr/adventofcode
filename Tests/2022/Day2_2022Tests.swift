//
//  Day2_2022Tests.swift
//  AdventOfCodeTests
//
//  Created by Emil Ramsdal on 02/12/2022.
//


import XCTest

final class Day2_2022Tests: XCTestCase, DayTest {
    
    var day: Day = Day2_2022(test: true)
    let data: [String] = ["A Y", "B X", "C Z"]
    
    
    func testTaskOne() throws {
        print("starting taskOne")
        
        let result = self.day.taskOne(dayData: data)
        
        XCTAssertEqual(result, 15)
    }
    
    func testTaskTwo() throws {
        print("starting taskTwo")
        
        let result = self.day.taskOne(dayData: data)
        
        XCTAssertEqual(result, 12)
    }
    
    
}
