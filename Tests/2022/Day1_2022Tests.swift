//
//  Day1_2022Tests.swift
//  AdventOfCodeTests
//
//  Created by Emil Ramsdal on 01/11/2022.
//

import XCTest

final class Day1_2022Tests: XCTestCase, DayTest {
    
    var day: Day = Day1_2022(test: true)
    
    let data = ["1000",
                "2000",
                "3000",
"",
                "4000",
"",
                "5000",
                "6000",
"",
                "7000",
                "8000",
                "9000",
"",
                "10000"]
    func testTaskOne() throws {
        print("starting taskOne")
        let result = self.day.taskOne(dayData: data)
        
        XCTAssertEqual(result, 24000)
    }
    
    func testTaskTwo() throws {
        print("starting taskTwo")
        let result = self.day.taskTwo(dayData: data)
        
        XCTAssertEqual(result, 45000)
    }
    

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }


}
