//
//  Day4_2022Tests.swift
//  AdventOfCodeTests
//
//  Created by Emil Ramsdal on 03/12/2022.
//

import XCTest

final class Day4_2022Tests: XCTestCase, DayTest {

    var day: Day = Day4_2022(test: true)
    
    let data = ["2-4,6-8",
                "2-3,4-5",
                "5-7,7-9",
                "2-8,3-7",
               "6-6,4-6",
                "2-6,4-8",]
    
    func testTaskOne() throws {
        print("starting taskOne")
        let result = self.day.taskOne(dayData: data)
        
        XCTAssertEqual(result, 2)
    }
    
    func testTaskTwo() throws {
        print("starting taskTwo")
        let result = self.day.taskTwo(dayData: data)
        
        XCTAssertEqual(result, 4)
    }


}
