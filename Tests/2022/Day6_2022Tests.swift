//
//  Day6_2022Tests.swift
//  AdventOfCodeTests
//
//  Created by Emil Ramsdal on 07/12/2022.
//

import XCTest

final class Day6_2022Tests: XCTestCase, DayTest {
   
    var day: Day = Day6_2022(test: true)
    
    let data = ["mjqjpqmgbljsphdztnvjfqwrcgsmlb"]
    
    func testTaskOne() throws {
        print("starting taskOne")
        let result = self.day.taskOne(dayData: data)
        
        XCTAssertEqual(result, 7)
    }
    
    func testTaskTwo() throws {
        print("starting taskTwo")
        let result = self.day.taskTwo(dayData: data)
        
        XCTAssertEqual(result, 19)
    }
    


    

}
