//
//  Day3Tests.swift
//  AdventOfCodeTests
//
//  Created by Emil Ramsdal on 02/12/2021.
//

import XCTest

class Day3Tests: XCTestCase, DayTest {

    var day: Day = Day3(test: true)
    let data = [
        "00100",
                "11110",
                "10110",
                "10111",
                "10101",
                "01111",
                "00111",
                "11100",
                "10000",
                "11001",
                "00010",
                "01010"
    ]

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testTaskOne() throws {
        print("starting taskOne")
        let result = self.day.taskOne(dayData: data)
        
        XCTAssertEqual(result, 198)
    }

    
    func testTaskTwo() throws {
        print("starting taskTwo")
        let result = self.day.taskTwo(dayData: data)
        
        print(result)
        XCTAssertEqual(result, 230)
    }


}
