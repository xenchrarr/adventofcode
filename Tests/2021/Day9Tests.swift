//
//  Day9Tests.swift
//  AdventOfCodeTests
//
//  Created by Emil Ramsdal on 09/12/2021.
//

import XCTest

class Day9Tests: XCTestCase, DayTest {
    
    var day: Day = Day9(test: true)
    let data = [
        "2199943210",
        "3987894921",
        "9856789892",
        "8767896789",
        "9899965678"
    ]

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testTaskOne() throws -> Void {
        print("starting taskOne")
        let result = self.day.taskOne(dayData: data)
        
        XCTAssertEqual(result, 15)
    }

    
    func testTaskTwo() throws -> Void {
        print("starting taskTwo")
        let result = self.day.taskTwo(dayData: data)
        
        XCTAssertEqual(result, 1134)
    }

}
