//
//  Day2Tests.swift
//  AdventOfCodeTests
//
//  Created by Emil Ramsdal on 01/12/2021.
//

import XCTest

class Day2Tests: XCTestCase, DayTest {
    
    var day: Day = Day2(test: true)
    let data = ["forward 5", "down 5", "forward 8", "up 3", "down 8", "forward 2"]

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testTaskOne() throws {
        print("starting taskOne")
        let result = self.day.taskOne(dayData: data)
        
        XCTAssertEqual(result, 4512)
    }

    
    func testTaskTwo() throws {
        print("starting taskTwo")
        let result = self.day.taskTwo(dayData: data)
        
        print(result)
        XCTAssertEqual(result, 900)
    }

}
