//
//  Day6Tests.swift
//  AdventOfCodeTests
//
//  Created by Emil Ramsdal on 06/12/2021.
//

import XCTest

class Day6Tests: XCTestCase, DayTest {
    
    var day: Day = Day6(test: true)
    let data = ["3,4,3,1,2"]

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testTaskOne() throws -> Void {
        print("starting taskOne")
        let result = self.day.taskOne(dayData: data)
        
        XCTAssertEqual(result, 5934)
    }

    
    func testTaskTwo() throws -> Void {
        print("starting taskTwo")
        let result = self.day.taskTwo(dayData: data)
        
        XCTAssertEqual(result, 26984457539)
    }

}
