//
//  Day7Tests.swift
//  AdventOfCodeTests
//
//  Created by Emil Ramsdal on 07/12/2021.
//

import XCTest

class Day7Tests: XCTestCase, DayTest {
    
    var day: Day = Day7(test: true)
    let data = ["16,1,2,0,4,2,7,1,2,14"]

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testTaskOne() throws -> Void {
        print("starting taskOne")
        let result = self.day.taskOne(dayData: data)
        
        XCTAssertEqual(result, 37)
    }

    
    func testTaskTwo() throws -> Void {
        print("starting taskTwo")
        let result = self.day.taskTwo(dayData: data)
        
        XCTAssertEqual(result, 168)
    }
    
    func testGetValue() throws -> Void {
        let day: Day7 = Day7(test: true)
        
        let result = day.getValue(number: 4, target: 5)
        
        XCTAssertEqual(result, 1)
    }

}

// 1-2-3
// 1-4 = 3
    // 1 + 2 + 3
