//
//  AdventOfCodeTests.swift
//  AdventOfCodeTests
//
//  Created by Emil Ramsdal on 01/12/2021.
//

import XCTest

class Day1Tests: XCTestCase, DayTest {
    
    var day: Day = Day1(test: true)
    let data = ["199", "200", "208", "210", "200", "207", "240", "269", "260", "263"]

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testTaskOne() throws -> Void {
        print("starting taskOne")
        let result = self.day.taskOne(dayData: data)
        
        XCTAssertEqual(result, 7)
    }

    
    func testTaskTwo() throws -> Void {
        print("starting taskTwo")
        let result = self.day.taskTwo(dayData: data)
        
        XCTAssertEqual(result, 5)
    }

}
